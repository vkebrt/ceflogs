package ceflogs

import (
	"time"
)

const (
	cTimeStampFormat = "Jan 2 15:04:05"
)

type CEFEvent struct {
	TimeStamp          time.Time
	TimeStampFormat    string
	Host               string
	CefVersion         uint
	DeviceVendor       string
	DeviceProduct      string
	DeviceVersion      float32
	DeviceEventClassID string
	Name               string
	Severity           string
	Extensions         []map[string]string
}

// NEWCEF - returns cleaned CEFEvenet object with basic vars setted up (cev version and timestamp format)
func NewCEF() *CEFEvent {
	cf := new(CEFEvent)
	cf.TimeStampFormat = cTimeStampFormat
	cf.CefVersion = 1
	return cf
}

// NewCEFTimestamp - returns cleaned CEFEvent with setted up timestamp and basic vars
func NewCEFTimestamp(timestamp time.Time) *CEFEvent {
	cf := new(CEFEvent)
	cf.TimeStamp = timestamp
	cf.TimeStampFormat = cTimeStampFormat
	cf.CeVersion = 1
	return cf
}

func (ce *CEFEvent) Marshal() (string, error) {
	// prepare values - time
	timeSTR := ce.TimeStamp.Format(cTimeStampFormat)
	// construct header of string
	eh := fmt.Sprintf("%s %s CEF:%d|%s|%s|%f|%s|%s|%s|",
		timeStr,
		ce.Host,
		ce.CefVersion,
		ce.DeviceVendor,
		ce.DeviceProduct,
		ce.DeviceVersion,
		ce.DeviceEventClassID,
		ce.Name,
		ce.Severity)
	return eh, nil
}
